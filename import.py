#! /usr/bin/env python3
import argparse
import json
import logging
import re
import os
import sys
import time
import requests                     #to download the HTML / XMLs from a Loxone Miniserver
from ftplib import FTP              #to download RAW statistic files from a Loxone Miniserver
from lxml import etree              #to parse / write the statistic files from a Loxone Miniserver
import influxdb                     #to send parsed statistic files to an InfluxDB
import struct                       #to interpret RAW statistic files from a Loxone Miniserver
from datetime import datetime       #to interpret RAW statistic files from a Loxone Miniserver
from bs4 import BeautifulSoup       #to interpret the HTML from a Loxone Miniserver that contains the list of stats files

try:
    from terminaltables import SingleTable
except ImportError:
    SingleTable = None

try:
    import argcomplete
except ImportError:
    argcomplete = None



def create_loxonic_xml(pathfilename, data):
    # Create the root element
    page = etree.Element('Statistics',
                         Name='defekt Primär Rücklauf', 
                         NumOutputs='1',
                         Outputs='%.1f'
                        )

    # Make a new document tree
    doc = etree.ElementTree(page)

    # Add the subelements
    for point in data:
        if(point is not None):
            pageElement = etree.SubElement(page, 'S', 
                                                  T=point[0],#'2019-02-10 14:00:00',
                                                  V='{:.1f}'.format(point[1])
                                          )

    # Save to XML file
    outFile = open(pathfilename, 'wb')
    doc.write(outFile,pretty_print=True, xml_declaration=True, encoding='utf-8') 
    outFile.close()
    
    return page


def read_raw_loxone_statistic_file(path):
    file = open(path, 'rb')
    # read the header
    header = file.read(32)
    # the header contains a struct
    # (2 bytes) number of values (2 bytes) magicnr,  (4 bytes) unknown, (4 bytes) textlength uint, (fixed length within 20 bytes) text chars
    nrofvalues, magicnr, unknown, length, title  = struct.unpack ("HHII20s",header)
    
    title=str(title[0:length].decode("utf-8"))

    # data is a struct: little endian
    # (4 bytes) uid,  (4 bytes) uint timestamp,  (8 bytes)    float v

    # timestamp Miniserver seconds since 1.1.2009, note: UNIX is since 1.1.1970, at UTC
    # so also subtract one hour by adding it the unix time
    a = datetime(2009,1,1,0,0,0)
    b = datetime(1970,1,1,1,0,0)
    deltats = (a-b).total_seconds()

    data = [] 
    while 1:
        buf = file.read(16)
        if not buf: break
        uuid, ts, val = struct.unpack ("IId",buf)
        if (uuid != 0):
            # increase ts with deltats
            dt_object = datetime.fromtimestamp(ts+deltats)
            date_time = dt_object.strftime("%Y-%m-%d %H:%M:%S")
            data.append([date_time, val])

    file.close()
    return title, data


def validate_config(config):
    if "miniserver" not in config:
        print("Miniserver not configured", file=sys.stderr)
        return False

    if "influxdb" not in config:
        print("InfluxDB not configured", file=sys.stderr)
        return False

    if "stats_map" not in config:
        print("Stats map not configured", file=sys.stderr)
        return False

    return True


def miniserver_request(config, path):
    request = requests.get("http://{}/{}".format(config["host"], path), auth=(config["username"], config["password"]))
    request.raise_for_status()

    return request


def get_files(miniserver_config):
    files = {}

    response = miniserver_request(miniserver_config, "stats/")
    #interpret the HTML in the response with BeautifulSoup, such that HTML special chars
    #starting with '&#x' are replaced (the 'x' in '$#x' tells you, that the character is represented as HEX Value)
    soup=BeautifulSoup(response.content, 'lxml')

    for tag in soup.findAll('a'):
        files[tag.attrs['href']] = tag.text #group 1 = Filename, group 2 = Title

    return files


def get_uuid_from_filename(filename):
    return re.search("([a-f0-9\-]+)", filename).group(1)


def import_stats(logger, filename, miniserver_config, influxdb_client, measurement, tags, value_map):
    try:
        #check if file has already been downloaded or uploaded
        if(os.path.exists('uploaded/'+filename)):
            logger.debug('Skipping: {} already uploaded'.format(filename))
            return 1            
        elif(os.path.exists('download/'+filename)):
            logger.info('reading XML file from download folder')
            content=open('download/'+filename, 'rb').read()
        elif(os.path.exists('ftp/'+filename.rstrip('.xml'))):
            logger.info('reading RAW file from ftp folder')
            try:
                logger.debug('parsing RAW file')
                title, data=read_raw_loxone_statistic_file('ftp/'+filename.rstrip('.xml'))
            except Exception as e:
                logger.error('Could not parse RAW file : {}'.format(e))
                
                file = open('error/'+filename, 'w')
                file.write('{}'.format(e))
                file.close()
                return 1
            try:
                logger.debug('creating XML File from RAW data')
                create_loxonic_xml('download/'+filename, data)
                content = open('download/'+filename, 'rb').read()
            except Exception as e:
                logger.error('Could not create XML file from RAW data: {}'.format(e))
                
                file = open('error/'+filename, 'w')
                file.write('{}'.format(e))
                file.close()
                return 1            
        elif(os.path.exists('error/'+filename)):
            logger.info('Skipping: {} error during last download'.format(filename))
            return 1
        else: #download an process
            try:
                logger.info('Starting download of XML')
                request = miniserver_request(miniserver_config, "stats/{}".format(filename))
                logger.debug('Filesize {}'.format(len(request.content)))
            
                file = open('download/'+filename, 'wb')
                file.write(request.content)
                file.close()
                
                content = request.content
            
            except Exception as e:
                logger.error('Downlod of XML failed --> try downloading RAW file')
                logger.info('Starting download of RAW file')
                #check if file has already been downloaded from ftp
                if(not os.path.exists('ftp/'+filename.rstrip('.xml'))):
                    try:
                        logger.debug('opening FTP session')
                        ftp = FTP(miniserver_config["host"])
                        ftp.login(miniserver_config["username"],miniserver_config["password"])
                        ftp.cwd('stats')
                        
                        file=filename.rstrip('.xml')
                        logger.debug('starting FTP download of stats/{}'.format(file))
                        ftp.retrbinary("RETR " + file ,open("ftp/" +file, 'wb').write)
                        ftp.close()

                        logger.debug('file {} downloaded via FTP'.format(file))
                        
                    except Exception as e:
                        logger.error('FTP Download Error: {}'.format(e))
                else:
                    logger.warning('RAW File already exists in ftp folder')
                try:
                    #parse file and create xml, so that the rest of the script works without additional clutter
                    logger.debug('parsing RAW file')
                    title, data=read_raw_loxone_statistic_file('ftp/'+filename.rstrip('.xml'))
                except Exception as e:
                    logger.error('Could not parse RAW file: {}'.format(e))
                    #catch
                    file = open('error/'+filename, 'w')
                    file.write('{}'.format(e))
                    file.close()
                    return 1
                try:
                    logger.debug('creating XML File from RAW data')
                    create_loxonic_xml('download/'+filename, data)
                    content = open('download/'+filename, 'rb').read()
                except Exception as e:
                    logger.error('Could create the XML: {}'.format(e))
                    #catch
                    file = open('error/'+filename, 'w')
                    file.write('{}'.format(e))
                    file.close()
                    return 1
        
        #parse the XML File
        tree = etree.fromstring(content)
        
        #create empty array to store dictionaries for the upload of multiple points to the DB
        points = []        
        
        #prepare the xml for the method 'influxdb_client.write_points'
        logger.info('Preparing XML for upload to DB'.format(len(points)))

        for stat in tree.iter("S"):
            time_string = stat.get("T")

            time_string = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(time.mktime(time.strptime(time_string, "%Y-%m-%d %H:%M:%S"))))

            fields = {}

            for attribute, field in value_map.items():
                value = float(stat.get(attribute))

                #logger.debug("Adding value {} from attribute '{}' to field '{}' with time {}".format(value, attribute, field, time_string))

                fields[field] = value

            points.append({
                "measurement": measurement,
                "tags": tags,
                "time": time_string,
                "fields": fields
            })
        
        #upload the points to the DB
        try:
            logger.info('Writing {} values to DB'.format(len(points)))
            influxdb_client.write_points(points, batch_size=5000)
            logger.debug("Data written")
            return 0
        #process Exception from uploading to DB
        except influxdb.exceptions.InfluxDBClientError as e:
            logger.warning('Python DB Client Error')
            logger.warning(e)

            exception_message = re.search('^(?P<response_code>[0-9]+): (?P<exception>\{.+\}).*$', str(e)) # e loks like '400 : {"error" : "error message"}'
            logger.warning('exception_message: {}'.format(exception_message))
            exception_response_code=exception_message.group('response_code')
            exception_content=eval(exception_message.group('exception'))
            
            logger.warning('Code: {} Content: {}'.format(exception_response_code, type(exception_content)))
            return 0
        
        
        except influxdb.exceptions.InfluxDBServerError as e:
            logger.warning('Influx DB Server Error {}'.format(e))
            
            if e['error'] == 'timeout':
                logger.warning('Influx DB Timeout -> retrying')
                influxdb_client.write_points(points, batch_size=5000)
                return 0
            else:
                logger.error('DB Server error occured while writing to DB: {}'.format(e))
                return 1
        
        except Exception as e:
            logger.debug('Error occured while writing: Exception Type: {}\nException Message: {}'.format(type(e), e))
            
    except KeyboardInterrupt:
        exit(1)
        return
    except Exception as e:
        logger.error("Exception occurred: {}".format(e))
        return 1

def main():
    argument_parser = argparse.ArgumentParser(description="Import statistics from Loxone Miniserver into InfluxDB")

    argument_parser.add_argument("--config", "-c", help="the configuration file to use (default: config.json)", default="config.json")
    argument_parser.add_argument("--list", "-l", action="store_true", help="only list stats files")
    argument_parser.add_argument("--quiet", "-q", action="store_true", help="only output warnings and errors")
    argument_parser.add_argument("--verbose", "-v", action="store_true", help="be more verbose")

    if argcomplete:
        argcomplete.autocomplete(argument_parser)

    arguments = argument_parser.parse_args()

    logger = logging.getLogger(__name__)

    if arguments.quiet:
        logger.setLevel(logging.WARNING)
    elif arguments.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    console_logger = logging.StreamHandler()
    console_logger.setFormatter(logging.Formatter("[%(asctime)s] %(levelname)s:%(name)s: %(message)s"))
    logger.addHandler(console_logger)

    try:
        with open(arguments.config, 'rb') as config_file:
            contents = config_file.read().decode("UTF8")
            config = json.loads(contents)
    except FileNotFoundError:
        argument_parser.print_help()
        print('\n\n\n')
        logger.error("configuration file not found, see readme for details")
        exit(1)
        return

    if not validate_config(config):
        exit(1)
        return

    miniserver_config = config["miniserver"]
    influxdb_config = config["influxdb"]
    stats_map = config["stats_map"]

    logger.info("Getting list of stats files from Miniserver")

    files = get_files(miniserver_config)

    logger.info("{} stats files to import".format(len(files)))

    if arguments.list:
        unique_stats = {}

        for filename, title in files.items():
            unique_stats[get_uuid_from_filename(filename)] = title

        stats_list = []

        for uuid, title in unique_stats.items():
            stats_list.append([uuid, re.search("(.*) [0-9]+$", title).group(1)])

        if SingleTable:
            table_rows = [
                ["UUID", "Name"]
            ]

            # noinspection PyCallingNonCallable
            print(SingleTable(table_rows + stats_list).table)
        else:
            for line in stats_list:
                print("\t".join(line))

        return

    influxdb_client = influxdb.InfluxDBClient(influxdb_config["host"], influxdb_config["port"], influxdb_config["username"], influxdb_config["password"], influxdb_config["database"])

    if(not os.path.exists('download')):
        os.mkdir('download')
    if(not os.path.exists('uploaded')):
        os.mkdir('uploaded')
    if(not os.path.exists('error')):
        os.mkdir('error')
    if(not os.path.exists('ftp')):
        os.mkdir('ftp')    
    
    for filename, title in files.items():
        logger.debug("Next: '{}'\nFilename: '{}'".format(title, filename))

        uuid = get_uuid_from_filename(filename)

        if uuid not in stats_map:
            #if no entry in stats_map (definde in the config file) is found, create a new entry
            match=re.search("(?P<name>.+?) \((?P<cat_room>.+?)\) (?P<year>[0-9]{4})(?P<month>[0-9]{2})", title)
            if match:
                name = match.group('name')
                stats_map[uuid]={"measurement":name}
                logger.warning("UUID '{}' not mapped, using name '{}' as InfluxDB measurement name".format(uuid, name))
            else:
                logger.warning("UUID '{}' not mapped, skipping".format(uuid))
                continue
            

        map_entry = stats_map[uuid]
            #example of stats_map <-> located in config.json
            #  "stats_map": {
            #    "0bc5a5cb-0384-8828-ffff504f94000000": {
            #      "measurement": "Temperature",
            #      "tags": {     #<<<<< TAGs are optional
            #        "room": "Bedroom"
            #      }
            #    },
            #    "0bc9716e-027e-9276-ffff504f94000000": {
            #      "measurement": "Power",
            #      "values": {   #<<<<< VALUEs are optional
            #        "V": "Total",
            #        "V2": "Current"
            #      }
            #    }
            #  }

        measurement = map_entry["measurement"]
                
        if "tags" in map_entry:
            tags = map_entry["tags"]
        else:
            tags = None

        if "values" in map_entry:
            value_map = map_entry["values"]
        else:
            value_map = {
                "V": "value"
            }

        logger.info( ("Next: '{}' Measurement '{}' " + '' if tags is None else " Tags {}").format(title, measurement, tags))

        success = import_stats(logger, filename, miniserver_config, influxdb_client, measurement, tags, value_map)
        
        #move uploaded file from dowload to upload folder
        if(os.path.exists('download/'+filename) and success==0):
            try:            
                os.rename('download/'+filename, 'uploaded/'+filename)
            except Exception as e:
                logger.error("Upload finished successfully. Moving file from folder 'download' to 'upload' falied: {} ".format(e))
                empty_file=open('uploaded/'+filename, 'w') #create empty file in uploaded folder - if the script is rerung the file will not be uploaded again.
                empty_file.close()

if __name__ == "__main__":
    main()
