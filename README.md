# Loxone Stats 2 InfluxDB

This script downloads all statistics collected by the Loxone Miniserver to a local folder and writes them to an InfluxDB instance.
Each statistic is imported in the InfluxDB instance as separate measurement.
RAW statistic files are downloaded and saved locally in a folder. The RAW Files are converted to 'loxonic' XML files.
If a statistic file has already been downloaded, it is not reloaded from the Loxone Miniserver.


## Requirements

* Python 3 with the following additional modules:
   * [Argcomplete](https://pypi.python.org/pypi/argcomplete) (optional) : `pip3 install argcomplete`
   * [InfluxDB-Python](https://github.com/influxdata/influxdb-python): `pip3 install influxdb`
   * [Requests](https://github.com/requests/requests): `pip3 install requests`
   * [terminaltables](https://github.com/Robpol86/terminaltables) (optional): `pip3 install terminaltables`
   * ftplib
   * lxml
   * BeautifulSoup
* Loxone Miniserver
* InfluxDB

## Configuration

A simple configuration file is required.

Just copy [config.sample.json](config.sample.json) to `config.json` and edit it to match your requirements.

The `influxdb` and `miniserver` configuration should be self explaining: It configures the connection to the InfluxDB and your Loxone Miniserver.

In the `stats_map` you have to map each UUID of the statistics to a measurement and (optional) tags.
If tags have to be added to the InfluxDB Measurements, this information can be written in the config-file.

## Usage

As you might not know the UUIDs of the stats, you can list all available statistics including their UUID and name. To do that just execute `python3 import.py --list`.

Without any arguments, the script will start to import the statistics configured in the configuration file: Just execute `python3 import.py`